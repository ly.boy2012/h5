import '../lib/resize';
import Common from '../common/index';
import Header from '../components/header/index';
import Footer from '../components/footer/index';
import './index.less'
export default class Index {
    constructor(name) {
        this.name = name;
    }
    init() {
        new Common().init();
        new Header().init();
        new Footer().init();

    }
}